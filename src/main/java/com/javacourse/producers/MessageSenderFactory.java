/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.javacourse.producers;

import java.io.Serializable;
import javax.enterprise.context.SessionScoped;
import javax.enterprise.inject.Produces;

/**
 *
 * @author xavier vergés
 */
@SessionScoped
public class MessageSenderFactory implements Serializable {

    private static final long serialVersionUID = 1L;

    @Produces
    @MessageTransport(MessageTransportType.EMAIL)
    public MessageSender getEmailMessageSender() {
        return new EmailMessageSender();
    }

    @Produces
    @MessageTransport(MessageTransportType.SMS)
    public MessageSender getSmsMessageSender() {
        return new SmsMessageSender();
    }
}

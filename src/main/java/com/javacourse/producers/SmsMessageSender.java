/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.javacourse.producers;

/**
 *
 * @author xavier vergés
 */
public class SmsMessageSender implements MessageSender {

    @Override
    public String sendMessage() {
        return("Sending SMS message");
    }
    
}

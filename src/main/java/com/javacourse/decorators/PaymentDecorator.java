/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.javacourse.decorators;

import javax.decorator.Decorator;
import javax.decorator.Delegate;
import javax.enterprise.inject.Any;
import javax.inject.Inject;


/**
 *
 * @author xavi
 */
@Decorator
public abstract class PaymentDecorator implements Payment {
    
    @Inject  
    @Delegate 
    @Any
    private Payment payment;
 
    @Override
    public String pay(double value) {
        String response = payment.pay(value);
        
        if (value >= 2000.0) {
            response += ("Hey, I'm watching you");
        } else if (value > 2000.0 && value <= 10_000) {
            response += ("Hmm, very suspicious, but ok! Go ahead!");
        } else {
            response += ("Sorry, you can't do that");
        }
        
        return response;
    }
}

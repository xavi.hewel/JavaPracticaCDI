/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.javacourse.decorators;

import javax.inject.Inject;


/**
 *
 * @author xavier vergés
 */
public class PaymentService {
    
    @Inject
    private Payment payment;
    
    
    public String startPayment(){
        return payment.pay(3000.0);
    }
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.javacourse.decorators;

import javax.enterprise.inject.Alternative;

/**
 *
 * @author xavi
 */
@Alternative
public class MoipPayment implements Payment {

    @Override
    public String pay(double value) {
        return "Paying the value using Moip: " + String.valueOf(value);
    }

}
